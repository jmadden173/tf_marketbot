import numpy as np
import xlsxwriter

workbook = xlsxwriter.Workbook('ItemData.xlsx')
marketplaceWorksheet = workbook.add_worksheet('Marketplace.tf')
steamMarketWorksheet = workbook.add_worksheet('Steam Market')

marketPlaceFileName = 'markeplceProfitArray.npy'
steamMarketFileName = 'steamMarketProfitArray.npy'

marketplace = np.load(marketPlaceFileName)
steam = np.load(steamMarketFileName)

row = 0
col = 0
i = 0

#name, marketplaceBuyPrice, steamSellPrice, potentialProfit, marketPlaceURL, steamURL   #How it should be formated in the excel sheet
marketplaceWorksheet.write(row, col, 'Item Name')
marketplaceWorksheet.write(row, col + 1, 'MarketPlace Buy Price')
marketplaceWorksheet.write(row, col + 2, 'Steam Sell Price')
marketplaceWorksheet.write(row, col + 3, 'Potential Profit')
marketplaceWorksheet.write(row, col + 4, 'Marketplace URL')
marketplaceWorksheet.write(row, col + 5, 'Steam URL')

row = 1

for i in range(0,len(marketplace)):
    marketplaceWorksheet.write(row, col, marketplace[i][0])
    marketplaceWorksheet.write(row, col + 1, marketplace[i][1])
    marketplaceWorksheet.write(row, col + 2, marketplace[i][2])
    marketplaceWorksheet.write(row, col + 3, marketplace[i][3])
    marketplaceWorksheet.write(row, col + 4, marketplace[i][4])
    marketplaceWorksheet.write(row, col + 5, marketplace[i][5])
    i += 1
    row += 1

row = 0
col = 0
i = 0

steamMarketWorksheet.write(row, col, 'Item Name')
steamMarketWorksheet.write(row, col + 1, 'MarketPlace Sell Price')
steamMarketWorksheet.write(row, col + 2, 'Steam Buy Price')
steamMarketWorksheet.write(row, col + 3, 'Potential Profit')
steamMarketWorksheet.write(row, col + 4, 'Marketplace URL')
steamMarketWorksheet.write(row, col + 5, 'Steam URL')

row = 1

for i in range(0,len(steam)):
    steamMarketWorksheet.write(row, col, steam[i][0])
    steamMarketWorksheet.write(row, col + 1, steam[i][1])
    steamMarketWorksheet.write(row, col + 2, steam[i][2])
    steamMarketWorksheet.write(row, col + 3, steam[i][3])
    steamMarketWorksheet.write(row, col + 4, steam[i][4])
    steamMarketWorksheet.write(row, col + 5, steam[i][5])
    i += 1
    row += 1

#Testing Area
print (marketplace[0][0])

workbook.close()
#print(data[0][0])
