import os
import numpy as np
import pandas as pd
from urllib.parse import urlencode
from urllib.parse import quote
import urllib
import urllib3
import json
from googlevoice import Voice
from googlevoice.util import input

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

http = urllib3.PoolManager()
tfMarketAPI = urlencode({'key': 'J5IBA09SGSFV7AKN9XMESLGU1H0RNZ69'})

def sendMessage(text):
    voice = Voice()
    voice.login()

    phoneNumber = input('Number to send message to: ')

    voice.send_sms(phoneNumber, text)

def getSteamItems(marketNameInput):
    steamAPI = urlencode({'api_key': '13bxSXCsfVr02KE0SvN2SGI8TfA'})
    steamMarket = 'http://api.steamapis.com/market/item/440/'
    item = marketNameInput
    item = item.replace(" ", "%20")
    #print (item)
    #print (steamMarket + marketNameInput + '?' + steamAPI)     For Debugging Purpses

    sellPrice = 0
    buyPrice = 0
    url = ''
    name = ''

    try:
        req = http.request('GET',steamMarket + item + '?' + steamAPI)
        data = json.loads(req.data.decode('utf-8'))
        formatedData = json.dumps(data, indent=4, sort_keys=True)
        try:
            sellPrice = data['histogram']['sell_order_summary']['price']
            buyPrice = data['histogram']['buy_order_summary']['price']
            url = data['url']
            name = data['market_hash_name']
        except:
            cannotFind = True
    except:
        print('Request Failed, Most likely due to a wierd or acented character')

    #print (formatedData)

    return sellPrice, buyPrice, url, name

numberOfDeals = urlencode({'num': '1000'})
skipNumber = urlencode({'skip': '1500'})

def getMarketplaceItems():
    marketPlaceDealsURL = 'https://marketplace.tf/api/Deals/GetDeals/v2?'
    req = http.request('GET', marketPlaceDealsURL + tfMarketAPI + '&' + numberOfDeals + '&' + skipNumber)
    data = json.loads(req.data.decode('utf-8'))
    formatedData = json.dumps(data, indent=4, sort_keys=True)
    #print (data)
    return data


def listCurrentItems():
    getDashboardItems = 'https://marketplace.tf/api/Seller/GetDashboardItems/v2?'
    req = http.request('GET', getDashboardItems + tfMarketAPI)
    data = json.loads(req.data.decode('utf-8'))
    data = pd.Series(data)
    #print (data.items)
    #return data

def main():

    marketplaceProfitArray = []
    steamMarketProfitArray = []

    for i in range(0,1000):
        print('====================================================================================================')
        marketplaceData = getMarketplaceItems()
        currentItem = marketplaceData['items'][i]['item']['name']
        marketplaceBuyPrice = marketplaceData['items'][i]['item']['lowest_price']
        marketplaceBuyPrice = (marketplaceBuyPrice)/100

        print ('Currently Checking: {}'.format(currentItem))
        print ('---------------------------------------------------------------------------------------------------')
        steamSellPrice, steamBuyPrice, steamURL, name = getSteamItems(currentItem)

        marketPlaceFileName = 'markeplceProfitArray.npy'
        steamMarketFileName = 'steamMarketProfitArray.npy'

        urlName = name.replace(" ", "%20")
        marketplaceURL = 'https://marketplace.tf/deals?sterm=' + urlName

        if (10 > marketplaceBuyPrice):

            potentialProfit = (steamSellPrice - marketplaceBuyPrice - (steamSellPrice * 0.15))

            print('Potential Profit is: {}'.format(potentialProfit))

            if (potentialProfit > 0):
                marketplaceDataArray = np.array([name, marketplaceBuyPrice, steamSellPrice, potentialProfit, marketplaceURL, steamURL])

                marketplaceProfitArray.append(marketplaceDataArray)
                np.save(marketPlaceFileName, marketplaceProfitArray)

                print(marketplaceProfitArray)

                print('Item Saved to an array')
                print('Item is profitable if bought from Marketplace.tf and sold on the Steam Market')
            elif (potentialProfit < 0):
                steamProfit = (marketplaceBuyPrice - steamBuyPrice - (marketplaceBuyPrice*.10))
                steamMarketDataArray = np.array([name, marketplaceBuyPrice, steamBuyPrice, steamProfit, marketplaceURL, steamURL])


                if (name != ''):
                    steamMarketProfitArray.append(steamMarketDataArray)
                    np.save(steamMarketFileName, steamMarketProfitArray)

                    print(steamMarketProfitArray)

                    print('Item Saved to an array')
                    print('Item is profitable if bought from the Steam Market and sold on Marketplace.tf')


                else:
                    print(steamMarketProfitArray)

                    print('Cannot find the item on the market, moving on to the next one')

        elif (10 < marketplaceBuyPrice):
            print('Price is too expensive! Moving to next item')
            print('The current price is: {}'.format(marketplaceBuyPrice))
        elif (cannotFind == True):
            print('Could not find {} on the marketplace, trying the next item on the list'.format(marketNameInput))
            cannotFind == False

        print('====================================================================================================')
        print('')
        print('')

        os.system('cls' if os.name == 'nt' else 'clear')

        i += 1

main()
